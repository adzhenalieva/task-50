class Machine {
    constructor() {
        this._turnedOn = false;
        let self = this;

        this.turnOn = () => {
            if (this._enabled === false) {
                console.log('You sholud plug In at first, then turn on');
            } else {
                self._turnedOn = true;
                console.log('Turned on');
            }
        };
        this.turnOff = () => {
            self._turnedOn = false;
            console.log('Turned off');
        };
    };
}

class HomeAppliance extends Machine {
    constructor() {
        super();

        this._enabled = false;
        let self = this;

        this.plugIn = () => {
            self._enabled = true;
            console.log('Pluged in');
        };

        this.plugOff = () => {
            self._enabled = false;
            console.log('Pluged off');
        }

    };
}

class WashingMachine extends HomeAppliance {
    constructor() {
        super();
    };

    run(regime) {
        console.log(`${regime} regime is set`);
    };
}

class LightSource extends HomeAppliance {
    constructor() {
        super();
    }

    setLevel(level) {
        if (this._enabled === true) {
            console.log(`Level of light is  ${level}`);
        } else {
            console.log('You sholud plug In at first, then set level')
        }
    };
}


class AutoVehicle extends Machine{
    constructor(){
        super();
        this.x = 0;
        this.y = 0;
    }

    setPosition(x, y){
        this.x = x;
        this.y = y;
        return this.x, this.y;
    };
}

class Car extends AutoVehicle{
    constructor(){
        super();
        this.speed = 10;
    }

    setSpeed(speed){
        this.speed = speed;
        console.log(`The speed is ${this.speed}`);
        return this.speed;
    };
    run(x, y){
        let newX = x;
        let newY = y;
        let speed = this.speed;
        let currentX = this.x;
        let currentY = this.y;
        console.log(`Car's current position is  ${currentX} ${currentY}`);
        let interval = setInterval(() => {
            if(currentX < newX){
                currentX = currentX + speed;
            }
            if (currentX > newX){
                currentX = (newX - currentX)+currentX;
            }
            if(currentY < newY){
                currentY = currentY + speed;
            }
            if (currentY > newY){
                currentY = (newY - currentY)+currentY;
            }
            console.log(`Car moved to these coordinates: ${currentX} ${currentY}`);
            if(currentX === newX && currentY === newY) {
                clearInterval(interval);
                console.log('Car stopped!');
            }
        }, 1000);
    };
}